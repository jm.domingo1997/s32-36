
const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

const auth = require(`./../auth.js`);
//User model
//const User = require(`./../models/User`);

//User Controller
const userController = require('./../controllers/userControllers');

//Syntax: router.HTTPmethod(`url`, <request listener>)
/*router.get('/', (req, res) => {
	//console.log(`Hello from userRoutes`);
	res.send(`Hello from userRoute`);
});*/

/*Routes would only handle requests & responses*/

// http://localhost:3001/users
router.post('/email-exists', (req, res) => {
	// invoke the function here
	userController.checkEmail(req.body.email)
	.then(result => res.send(result));
});

router.post('/register', (req, res) => {
	//
	userController.register(req.body).then(result => res.send(result))
});

/*Mini activity
-create a route to get all users
*/

router.get('/', (req,res) => {
	userController.getAllUsers().then(result => res.send(result));
})

router.post('/login', (req, res) => {
	userController.login(req.body).then(result => res.send(result));
})

router.get('/details', auth.verify, (req, res) => {
	//console.log(req.headers.authorization);
	
	//console.log(auth.decode(req.headers.authorization));
	
	let userData = auth.decode(req.headers.authorization);

	userController.getUserProfile(userData).then(result => res.send(result));
})

//edit user information
router.put('/:userId/edit', (req, res) => {
	//console.log(req.params);
	//console.log(req.body)
	const userId = req.params.userId;
	//console.log(userId)
	userController.editProfile(userId, req.body).then(result => res.send(result))
})

router.put('/edit', auth.verify,(req, res) => {
	//console.log(req.headers.authorization);
	let userId = auth.decode(req.headers.authorization).id
	//console.log(payload.id);
	//console.log(userId)
	userController.editUser(userId, req.body).then(result => res.send(result));
})

/*create a route to update user details with the following
endpoint = "/edit-profile"
function name: editDetails
method: put
use email as filter to findOne() method
*/

router.put('/edit-profile', (req,res) => {
	//console.log(req.body);
	
	userController.editDetails(req.body).then(result => res.send(result));

})

//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/:userId/delete"
		// function name: delete()
		// method: delete
		// use id as filter to findOneAndDelete method

router.delete('/:userId/delete', (req, res) => {
	const userId = req.params.userId;
	//console.log(req.params);
	userController.delete(userId).then(result => res.send(result));
})


//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/delete"
		// function name: deleteUser()
		// method: delete
		// use email as filter to findOneAndDelete method
		// return true if document was successfully deleted

router.delete('/delete', (req,res) => {
	//const userEmail = req.body.email;
	//console.log(auth.decode(req.headers.authorization).email);
	const userEmail = auth.decode(req.headers.authorization).email
	userController.deleteUser(userEmail).then(result => res.send(result));
})

//enrollments
router.post("/enroll", auth.verify, (req,res) => {

	let data = {
		//user id of the logged in user
		userId :auth.decode(req.headers.authorization).id,
		//course id of the course you're enrolling in, to be supplied
		courseId: req.body.courseId
	}

	userController.enroll(data).then(result => res.send(result));
})

module.exports = router;