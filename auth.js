
//json web tokenn
	// sign - function that creates a token
	//  verify - functiom that checks if there's presence of token
	// decode - function that decodes the token

const jwt = require('jsonwebtoken');
const secret = "secret"

module.exports.createAccessToken = (user) => {
	//user = object because it came from the matching document of the user from the database

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//jwt.sign(payload , secret, {});

	return jwt.sign(data, secret, {});
}

module.exports.decode = (token) => {
	//console.log(token) //token
	//jwt.decode(token, {})

	let slicedToken = token.slice(7, token.length);
	//console.log(slicedToken);

	//console.log( jwt.decode(slicedToken , {complete: true}) );

	return jwt.decode(slicedToken)
}

module.exports.verify = (req, res, next) => {

	// where to get the token?
	let token = req.headers.authorization;

	//console.log(token);
	// jwt.verify(token, secret, function)

	if(typeof token !== "undefined"){
		let slicedToken = token.slice(7, token.length);
		return jwt.verify(slicedToken, secret, (err, data) => {
			if(err){
				res.send( {auth: "failed"});
			} else{
				next()
			}
		})
	}else {
		res.send(false);
	}
}