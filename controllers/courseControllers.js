const Course = require('./../models/Course');

//create courses -done
module.exports.createCourse = (reqBody) => {
	//const {courseName, description, price} = reqBody;
	let newCourse = new Course({
			courseName: reqBody.courseName,
			description: reqBody.description,
			price: reqBody.price
	 });

	//console.log(newCourse)
	return newCourse.save().then((result,error) => {
		if(result){
			return true;
		}else{
			return false;
		}
	})
}
//retrieve all course -done
module.exports.getAllCourses = () =>{
	return Course.find({}).then( (result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
}
//retrieving only active courses -done
module.exports.getActiveCourses = () =>{
	return Course.find({isActive:true}).then((result,error) =>{
		if(result){
			return result;
		}else{
			return error;
		}
	})
}
//get a specific course using findOne() -done
module.exports.getSpecificCourse = (courseName) =>{
	return Course.find({courseName:courseName}).then((result,error) =>{
		if(result){
			return result;
		}else{
			return error;
		}
	})
}
//get specific course using findById()
module.exports.getCourseById = (courseId) =>{
	return Course.findById(courseId).then((result,error)=>{
		if(result){
			return result;
		}else{
			return error;
		}
	})
}